#pragma once
#include <gtk/gtk.h>
union gtk_suitcase_slot {
	char  * large_text [500];
	int num;
	unsigned unum;
};

struct gtk_suitcase {
	union gtk_suitcase_slot spot_A;
	union gtk_suitcase_slot spot_B;
	union gtk_suitcase_slot spot_C;
	union gtk_suitcase_slot spot_D;

};

void destroy(GtkWidget *, gpointer);
GtkWidget * get_file_menu(void);
GtkWidget * get_edit_menu(void);
GtkWidget * get_help_menu(void);


static inline void create_window (GtkWidget ** window, const char * title, unsigned border_width, unsigned width, unsigned height ) {

	*window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	g_signal_connect(window, "destroy", G_CALLBACK (destroy), NULL);
	gtk_window_set_title (GTK_WINDOW(*window), title);
	gtk_container_set_border_width(GTK_CONTAINER (*window), border_width);
	gtk_widget_set_size_request (GTK_WIDGET (*window), width, height);

}	
