#include "interface.h"

GtkWidget * get_file_menu (void) {
	GtkItemFactory *item_factory;
	GtkWidget *button, *menu;
	item_factory = gtk_item_factory_new (GTK_TYPE_MENU, "<main>", NULL);
	gtk_item_factory_create_items (item_factory, nmenu_items, menu_items, NULL);
	menu = gtk_item_factory_get_widget (item_factory, "<main>");
	button = gtk_button_new_with_label ("_FIlE_");
	g_signal_connect (button, "event",G_CALLBACK(popup_cb),	(gpointer) menu);
	return  button;
}