
#include "interface.h"


/*
void button_pressed(GtkWidget * widget, gpointer data){
	struct gtk_suitcase * package = (struct gtk_suitcase*)data;
	g_print("%s\n", *(package->spot_A.large_text));
}
*/

void destroy(GtkWidget * widget, gpointer data){
	gtk_main_quit();
}

void open_project (GtkWidget * gtkw, gpointer data) {
	g_message("Opening file manager\n");
}

void save_project (GtkWidget * gtkw, gpointer data) {
	g_message("Saving project\n");
}

void quit_project (GtkWidget * gtkw, gpointer data) {
	save_project(gtkw, data);
	g_message("Quiting project\n");
	destroy(gtkw, data);
}

void export_project (GtkWidget * gtkw, gpointer data) {
	g_message("Exporting project\n");
}

void undo_action (GtkWidget * gtkw, gpointer data) {
	g_message("Inside: %s\n", __func__);
}

void redo_action (GtkWidget * gtkw, gpointer data) {
	g_message("Inside: %s\n", __func__);
}

void __copy (GtkWidget * gtkw, gpointer data) {
	g_message("Inside: %s\n", __func__);
}

void __paste (GtkWidget * gtkw, gpointer data) {
	g_message("Inside: %s\n", __func__);
}

void get_help (GtkWidget * gtkw, gpointer data) {
	g_message("Inside: %s\n", __func__);
}

void get_info (GtkWidget * gtkw, gpointer data) {
	g_message("Inside: %s\n", __func__);
}

void get_index (GtkWidget * gtkw, gpointer data) {
	g_message("Inside: %s\n", __func__);
}

static GtkItemFactoryEntry menu_items[] = {
	{	"/_File",			NULL,				NULL,				0,	"<Branch>"} ,
	{	"/File/Open",		"<control>O",		open_project,		0,	"<StockItem>", GTK_STOCK_OPEN} ,
	{	"/File/Save",		"<control>S",		save_project,		0,	"<StockItem>", GTK_STOCK_SAVE} ,
	{	"/File/Quit",		"<control>Q",		quit_project,		0,	"<StockItem>", GTK_STOCK_QUIT} ,
	{	"/File/Export",		NULL,				export_project,		0,	"<StockItem>", GTK_STOCK_QUIT} ,
	{	"/_Edit",			NULL,				NULL,				0,	"<Branch>"} ,
	{	"/Edit/Undo",		"<control>Z",		undo_action, 		0,	"<StockItem>", GTK_STOCK_UNDO} ,
	{	"/Edit/Redo",		"<control>R",		redo_action,		0,	"<StockItem>", GTK_STOCK_REDO} ,
	{	"/Edit/Copy",		"<control>C",		__copy,				0,	"<StockItem>", GTK_STOCK_COPY} ,
	{	"/Edit/Paste",		"<control>V",		__paste,			0,	"<StockItem>", GTK_STOCK_PASTE} ,
	{	"/_Help",			NULL,				NULL,				0,	"<Branch>"} ,
	{	"/Help/Get Help",	NULL,				get_help,			0,	"<StockItem>", GTK_STOCK_HELP} ,
	{	"/Help/Info",		NULL,				get_info,			0,	"<StockItem>", GTK_STOCK_INFO} ,
	{	"/Help/Index",		NULL,				get_index,			0,	"<StockItem>", GTK_STOCK_INDEX} ,
};


static gint number_menu_items = sizeof menu_items/sizeof menu_items[0];

static gboolean popup_cb( GtkWidget *widget, GdkEvent *event, GtkWidget *menu ){
   GdkEventButton *bevent = (GdkEventButton *)event;
  
   /* Only take button presses */
   if (event->type != GDK_BUTTON_PRESS)
     return FALSE;
  
   /* Show the menu */
   gtk_menu_popup (GTK_MENU(menu), NULL, NULL,
                   NULL, NULL, bevent->button, bevent->time);
  
   return TRUE;
}

GtkWidget * get_file_menu (void) {
	GtkItemFactory *item_factory;
	GtkWidget *button, *menu;
	item_factory = gtk_item_factory_new (GTK_TYPE_MENU, "<main>", NULL);
	gtk_item_factory_create_items (item_factory, number_menu_items, menu_items, NULL);
	menu = gtk_item_factory_get_widget (item_factory, "<main>");
	button = gtk_button_new_with_label ("file");
	g_signal_connect (button, "event",G_CALLBACK(popup_cb),	(gpointer) menu);
	return  button;
}

GtkWidget * get_edit_menu (void) {
	GtkItemFactory *item_factory;
	GtkWidget *button, *menu;
	item_factory = gtk_item_factory_new (GTK_TYPE_MENU, "<main>", NULL);
	gtk_item_factory_create_items (item_factory, number_menu_items, menu_items, NULL);
	menu = gtk_item_factory_get_widget (item_factory, "<main>");
	button = gtk_button_new_with_label ("edit");
	g_signal_connect (button, "event",G_CALLBACK(popup_cb),	(gpointer) menu);
	return  button;
}

GtkWidget * get_help_menu (void) {
	GtkItemFactory *item_factory;
	GtkWidget *button, *menu;
	item_factory = gtk_item_factory_new (GTK_TYPE_MENU, "<main>", NULL);
	gtk_item_factory_create_items (item_factory, number_menu_items, menu_items, NULL);
	menu = gtk_item_factory_get_widget (item_factory, "<main>");
	button = gtk_button_new_with_label ("help");
	g_signal_connect (button, "event",G_CALLBACK(popup_cb),	(gpointer) menu);
	return  button;
}

static GtkWidget * get_menubar_menu ( GtkWidget * window) {
	GtkItemFactory *item_factory;
	GtkAccelGroup * accel_group;
	accel_group = gtk_accel_group_new ();
	item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>", accel_group);

	gtk_item_factory_create_items (item_factory, number_menu_items, menu_items, NULL);

	gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

	return gtk_item_factory_get_widget (item_factory, "<main>");
}

#define DROP_DOWN_LENGTH 3






int main (int argc, char **argv) {


	GtkWidget * window;
	GtkWidget * mainMenu;
	GtkWidget * fileMenu;
	GtkWidget * editMenu;
	GtkWidget * helpMenu;
	GtkWidget * vbox_;
	GtkWidget * hbox_shelf_right_top;
		GtkWidget * vbox_right;
			GtkWidget * btn1;
			GtkWidget * btn2;
		GtkWidget * display_area;
	gtk_init(&argc, &argv);

	create_window(&window, "Set Theory", 5, 500, 500);

	display_area = gtk_drawing_area_new();
	gtk_widget_set_size_request (display_area, 100, 100);
//	btn1 = gtk_button_new_with_label("New Set");
//	btn2 = gtk_button_new_with_label("New Member");
	

	mainMenu = get_menubar_menu (window);
	fileMenu = get_file_menu();// gtk_menu_new();
	editMenu = get_edit_menu();
	helpMenu = get_help_menu();

	vbox_right = gtk_vbox_new(FALSE, 1);
	vbox_ = gtk_vbox_new(FALSE, 1);

//	gtk_container_set_border_width (GTK_CONTAINER (vbox_right), 1);
//	gtk_container_add (GTK_CONTAINER (vbox_), vbox_right);
//	gtk_widget_show(vbox_right);

//	gtk_container_set_border_width (GTK_CONTAINER (vbox_), 1);
	gtk_container_add (GTK_CONTAINER (window), vbox_);
//	gtk_widget_show(vbox_);
	
//	hbox_shelf_right_top = gtk_hbox_new(FALSE, 0);
//	gtk_container_set_border_width (GTK_CONTAINER (hbox_shelf_right_top), 1);




//	gtk_container_add (GTK_CONTAINER (hbox_shelf_right_top), display_area );
//	gtk_container_add (GTK_CONTAINER (hbox_shelf_right_top), vbox_right);
//	gtk_container_add (GTK_CONTAINER (vbox_right), btn1 );
//	gtk_container_add (GTK_CONTAINER (vbox_right), btn2 );
//	gtk_widget_show(hbox_shelf_right_top );
	
	
	gtk_box_pack_start (GTK_BOX (vbox_), mainMenu, FALSE, TRUE, 0);
//	gtk_box_pack_end (GTK_BOX (vbox_), hbox_shelf_right_top, FALSE, TRUE, 0);
//	gtk_box_pack_end (GTK_BOX (vbox_), fileMenu, FALSE, TRUE, 0);
//	gtk_box_pack_end (GTK_BOX (vbox_), editMenu, FALSE, TRUE, 0);
//	gtk_box_pack_end (GTK_BOX (vbox_), helpMenu, FALSE, TRUE, 0);

 	gtk_widget_show_all (window);
	gtk_main ();
	return 0;
}



